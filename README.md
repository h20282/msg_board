# 网页留言板

## 构建（需要安装 [Conan](https://conan.io/downloads.html) 和 [CMake](https://cmake.org/download/) ）

```shell
mkdir build
cd build
conan install ..
cmake ..
cmake --build .
./bin/msg_board_server.exe
```
