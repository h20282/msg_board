#include <ctime>
#include <iostream>
#include <set>
#include <utility>
#include <vector>

#include <fmt/format.h>
#include <nlohmann/json.hpp>
#include <websocketpp/config/asio_no_tls.hpp>
#include <websocketpp/server.hpp>

using server = websocketpp::server<websocketpp::config::asio>;

server print_server;
std::vector<std::pair<std::time_t, std::string>> msgs;
std::map<std::string, websocketpp::connection_hdl> clients;

std::string GetUuid(websocketpp::connection_hdl hd) {
    try {
        auto addr = print_server.get_con_from_hdl(hd)
                            ->get_socket()
                            .remote_endpoint()
                            .address()
                            .to_string();
        auto port = print_server.get_con_from_hdl(hd)
                            ->get_socket()
                            .remote_endpoint()
                            .port();
        return fmt::format("[{}:{}]", addr, port);
    } catch (std::exception &e) { std::cout << e.what() << std::endl; }
}

void Send() {
    auto arr = nlohmann::json(msgs).dump();
    for (auto &pair : clients) {
        print_server.send(pair.second, arr,
                          websocketpp::frame::opcode::value::text);
    }
}

void on_message(websocketpp::connection_hdl hd, server::message_ptr msg_p) {
    auto uuid = GetUuid(hd);
    clients.emplace(uuid, hd);

    auto msg = msg_p->get_payload();
    msgs.push_back(std::make_pair(std::time(0), uuid + ":" + msg));
    Send();
}

void OnClose(websocketpp::connection_hdl hd) {
    clients.erase(GetUuid(hd));
}

void OnOpen(websocketpp::connection_hdl hd) {
    clients.emplace(GetUuid(hd), hd);
    Send();
}

int main() {
    print_server.set_message_handler(&on_message);
    print_server.set_open_handler(&OnOpen);
    print_server.set_close_handler(&OnClose);
    print_server.set_access_channels(websocketpp::log::alevel::all);
    print_server.set_error_channels(websocketpp::log::elevel::all);

    print_server.init_asio();
    print_server.listen(9002);
    print_server.start_accept();

    print_server.run();
}